package com.example.debtrerrangement.debtInfo


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import com.example.debtrerrangement.R
import kotlinx.android.synthetic.main.fragment_add_debt.*
import java.util.ArrayList


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class AddDebtFragment : Fragment(){

    var spinner: Spinner? = null

    companion object {
        val bankList: List<String> = object : ArrayList<String> () {
            init {
                add("Bangkok Bank")
                add("Kasikorn")
                add("Krung Thai Bank")
                add("The Siam Commercial Bank")
                add("TMB Bank")
                add("Bank of Ayudhya")
            }
        }

        val rateTypeList: List<String> = object  : ArrayList<String> () {
            init {
                add("Effective Rate")
                add("Flat Rate")
                add("Fixed Rate")
                add("MRR")
                add("MOR")
                add("MLR")
                add("Student Rate")
                add("Credit Card")
                add("Ceiling")
                add("Default")
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        activity?.setTitle(R.string.add_debt)

        return inflater.inflate(R.layout.fragment_add_debt, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    //    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//
//    override fun onNothingSelected(parent: AdapterView<*>?) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//
//    private fun bankListSpinner() {
//        var banksList = arrayOf("Bangkok Bank", "Kasikorn Bank",
//            "Krung Thai Bank", "The Siam Commercial Bank", "TMB Bank", "Bank of Ayudhya")
//
//        spinner = this.financial_name_spinner
//        spinner!!.setOnItemSelectedListener(this)
//
//        val adapter = ArrayAdapter( this,
//            R.layout.support_simple_spinner_dropdown_item,
//            banksList
//        )
//
//        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
//        spinner!!.setAdapter(adapter)
//
//
//    }
//
//    private fun rateTypeListSpinner() {
//
//        var rateTypeList = arrayOf("Effective Rate", "Flat Rate", "Fixed Rate",
//            "MRR", "MOR", "MLR", "Student Rate", "Credit Card", "Ceiling", "Default")
//
//    }

}
